from unittest import TestCase


class Empleados:
    """Los metodos se suelen poner en minuscula"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calculo_impuestos(self):
        return self.nomina * 0.3

    def __str__(self):
        return "El empleado{name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.calculo_impuestos())


class TestEmpleado(unittest, TestCase):

    def test_construir(self):
        e1 = Empleados("nombre", 5000)
        self.assertEqual(e1.nomina, 5000)
         
    def test_impuestos(self):
        e1 = Empleados("nombre", 5000)
        self.assertEqual(e1.calculo_impuestos(), 5)

    def test_str(self):
        e1 = Empleados("pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000", e1.__strs__())

if __name__ == "__main__":
    inttest.main()

empleadoPepe = Empleados("Pepe", 20000)

empleadoAna = Empleados("Ana", 30000)

total = empleadoAna.calculo_impuestos() + empleadoPepe.calculo_impuestos()

print (empleadoAna)
print (empleadoPepe)

print("Los impuestos a pagar en total son {:.2f} euros".format(total))
