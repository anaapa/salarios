class Empleado:
    """Los metodos se suelen poner en minuscula"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calculo_impuestos(self):
        return self.nomina * 0.3

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.calculo_impuestos())

class Jefe(Empleado):
    def __init__(self, n, s, b=0):
        super().__init__(n, s)
        self.extra = b

    def calculo_impuestos(self):
        return (self.nomina + self.extra) * 0.3

    def calculo_impuestos2(self):
        return (super().calculo_impuestos() + self.extra) * 0.3

    ##calculo_impuestos2 es mas util, si se cambia a clase padre no haria falta modificar ambas

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.calculo_impuestos2())


e1 = Empleado("Pepe", 20000)
j1 = Jefe("Ana", 30000, 2000)
